


import pandas as pd

def get_titanic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_title(name):
    # Common titles within the Titanic dataset
    if 'Mr.' in name:
        return 'Mr.'
    elif 'Mrs.' in name:
        return 'Mrs.'
    elif 'Miss.' in name:
        return 'Miss.'
    else:
        # This is a simple example and does not handle other titles like 'Master', 'Dr', etc.
        return 'Other'

def get_filled():
    df = get_titanic_dataframe()
    

    df['Title'] = df['Name'].apply(get_title)
    
   
    median_ages_before = {
        'Mr.': df[df['Title'] == 'Mr.']['Age'].median(),
        'Mrs.': df[df['Title'] == 'Mrs.']['Age'].median(),
        'Miss.': df[df['Title'] == 'Miss.']['Age'].median()
    }
    
    
    df['Age'] = df.apply(
        lambda row: median_ages_before[row['Title']] if pd.isnull(row['Age']) and row['Title'] in median_ages_before else row['Age'],
        axis=1
    )
    
    
    median_ages_after = {
        'Mr.': df[df['Title'] == 'Mr.']['Age'].median(),
        'Mrs.': df[df['Title'] == 'Mrs.']['Age'].median(),
        'Miss.': df[df['Title'] == 'Miss.']['Age'].median()
    }
    
    
    filled_ages = [
        ('Mr.', median_ages_before['Mr.'], median_ages_after['Mr.']),
        ('Mrs.', median_ages_before['Mrs.'], median_ages_after['Mrs.']),
        ('Miss.', median_ages_before['Miss.'], median_ages_after['Miss.'])
    ]
    
    return filled_ages


filled_ages_info = get_filled()
print(filled_ages_info)

